//
//
//
//
//
//
// getting element of input type text.
const inputAmount = document.getElementById("inputAmount");
const inputRoi = document.getElementById("inputRoi");
const inputYears = document.getElementById("inputYears");

/// getting element of input type range sliders.
const myAmount = document.getElementById("myAmount");
const myRoi = document.getElementById("myRoi");
const myYears = document.getElementById("myYears");

// getting element of text tab.
const tabEmi = document.getElementById("r1");
const tabP_Amount = document.getElementById("r2");
const tabInterest = document.getElementById("r3");
const tabTotal = document.getElementById("r4");

/// setting eventListner to input type text.
inputAmount.addEventListener("input", setRangeValue);
inputRoi.addEventListener("input", setRangeValue);
inputYears.addEventListener("input", setRangeValue);

// setting eventListner to input type range slider.
myAmount.addEventListener("input", setInputValue);
myRoi.addEventListener("input", setInputValue);
myYears.addEventListener("input", setInputValue);

async function setInputValue(e) {
  //getting values from input range sliders.
  const principalAmaount = myAmount.value;
  const roiValue = myRoi.value;
  const yearsValue = myYears.value;

  // setting input value from input range sliders.
  inputAmount.value = principalAmaount;
  inputRoi.value = roiValue;
  inputYears.value = yearsValue;
  const rangeValues = { principalAmaount, roiValue, yearsValue };
  showTotabBox(rangeValues);
  return rangeValues;
}

async function setRangeValue(e) {
  // getting value from input .
  const principalAmaount = inputAmount.value;
  const roiValue = inputRoi.value;
  const yearsValue = inputYears.value;

  // setting values from input to range sliders.
  myAmount.value = principalAmaount;
  myRoi.value = roiValue;
  myYears.value = yearsValue;
  const rangeValues = { principalAmaount, roiValue, yearsValue };
  showTotabBox(rangeValues);
  return rangeValues;
}

function calculateEmi(values) {
  const r = values.roiValue / 12 / 100;
  const p = values.principalAmaount;
  const n = values.yearsValue * 12;

  const top = Math.pow(1 + r, n);
  const bottom = top - 1;
  // const finalEMI = (
  //   (p * r * Math.pow(1 + r, n)) /
  //   (Math.pow(1 + r, n) - 1)
  // ).toFixed(0);
  const ratio = top / bottom;
  const finalEMI = (p * r * ratio).toFixed(0);
  const totelIntersert = finalEMI * n - p;
  const totalPayment = totelIntersert + parseFloat(p);
  return { finalEMI, totelIntersert, totalPayment };
}

const data = {
  labels: ["Monthly EMI", "Total Interest", "Principal Amount"],
  datasets: [
    {
      data: [310, 54, 120],
      backgroundColor: [
        "rgb(54,162,235)",
        "rgb(255,205,86)",
        "rgb(255,99,132)",
      ],
      hoverOffset: 50,
    },
  ],
};

const config = {
  type: "pie",
  data: data,
  options: {
    responsive: true,
    title: {
      display: true,
      text: "Loan EMI Calculator",
    },
  },
};

const myChart = new Chart(
  document.getElementById("myChart").getContext("2d"),
  config
);
async function showTotabBox(valueObj) {
  const { finalEMI, totelIntersert, totalPayment } = calculateEmi(valueObj);
  tabEmi.textContent = finalEMI;
  tabInterest.textContent = totelIntersert;
  tabP_Amount.textContent = valueObj.principalAmaount;
  tabTotal.textContent = totalPayment;

  const dataArray = Object.values({ finalEMI, totelIntersert, totalPayment });
  data.datasets[0].data = dataArray;
  myChart.update();
}

setInputValue();
